import StrategyList from "components/strategy/strategy-list";
import React from "react";

const StrategyListC = () => {
  return (
    <>
      <StrategyList isPublic={true} />
    </>
  );
};

export default StrategyListC;
