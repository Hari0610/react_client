import ReportTable from "components/data-display/ReportTable";
import { SubTitle } from "components/data-display/Typo";
import React from "react";
import styled from "styled-components";

interface Indexable {
  [idx: string]: string;
}

interface IDetailSummary {
  header: string[];
  body: Indexable[];
  props?: any;
}

const DetailSummary: React.FC<IDetailSummary> = ({
  header,
  body,
  ...props
}) => {
  return (
    <SDetailSummary
      {...props}
      className="articleDetailSummary"
      style={{ marginBottom: "100px" }}
    >
      <div className="flexRow" style={{ marginTop: "50px" }}>
        <SubTitle title="상세 리포트" style={{ margin: "20px 0px" }} />
      </div>
      <ReportTable body={body} header={header} />
    </SDetailSummary>
  );
};

export default DetailSummary;

const SDetailSummary = styled.section``;
