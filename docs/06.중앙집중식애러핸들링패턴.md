## ref

https://ui.toast.com/weekly-pick/ko_20200623

필요 개념

- React-router-dom
- 서버 애러 코드 ( 400, 404, 500 )
- React ContextAPI

# 중앙집중식 애러 핸들링

## 목적

- 애러 코드 404,403,500 에 대해 어떻게 대처 해야 하는가
- 한 곳에서 모두 처리하는것이 이상적이다.
- 어떤 상태관리 라이브러리든( apollo, redux, recoil 이든 ) 상관없이 구현되어야 함
- 중앙집중식으로 확장가능한 애러 처리방법 제시 해야 함

## 404 처리하기

### 상황

1.잘못된 url 정규식을 입력했을때  
2.유효한 url정규식을 통과했지만, 품종검색결과 없을때

### 해결

1. Switch컴포넌트를 이용해서, 결국 아무것도 걸리지 않는다면  
   Page404를 랜더링 하도록 한다.
2. useEffect을 이용해서 , API 호출 결과 애러코드가 나오면, 404 페이지를  
   랜더링 하도록 if문을 건다.

### 해결.2의 문제점

1. 중첩 컴포넌트에서 404 처리

- 컴포넌트 트리에서 깊게 위치한다면, 일부 컴포넌트만 404 라고 뜰것이다.
- 다른 컴포넌트 요소들은 정상(비정상)적으로 랜더링이 될 것이다.

2. 반복적인 코드와 로직

- 매번 API의 응답결과를 관찰하고, if문으로 404페이지를 랜더링 해주는 코드를 작성

3. 여러응답 애러 다루기

   - 404는 NOT FOUND 자원검색 실패지만, 다른 애러는 어떻게 처리를 하는가?
   - 이는 2번문제와 함께 더 유연하지 못한 결과를 초래한다.

4. API호출이 외부 컴포넌트에서 온 경우  
   Redux - thunk, saga, observable 에서 온 데이터를 가지고 온 경우

## 다른 해결 방법

1. Redirect 사용하기

- 여전히 문제다
- 애러가 발생하면, 404 page를 보여주는 url로 보내게 되는것이다.
- 애러가 난 상황을 잃어버리게 된다.
- 404 page 가 뜨면서, 잘못입력한 상황을 가지고 있을 순 없을까?

2. 공통 로직을 hook으로 관리하기

- Fetch 이후 상태 코드를 관리하는 부분까지 hook으로 만든다.
- 코드 중복은 해결되었지만, 여전히 상태코드별 랜더링을 어떻게?

3. render-props 사용하기  
   Ref) https://react-etc.vlpt.us/04.render-prop.html

기존

```
- 컴포넌트는 children 속성이 있고, JSX를 보통 받는다.
```

개선 테크닉

```
- React 컴포넌트 간에 코드를 공유하기 위해, props를 이용하는 간단한 테크닉이다.
```

- eg)

```ts
<DataProvider render={(data) => <h1>Hello {data.target}</h1>} />
```

- eg)

```ts
const Query = ({ url }) => {
  const { data, statusCode } = useQuery({ url });
  if (statusCode === 404) {
    return <Page404 />;
  }
  // ... 등 여기서 여러 HTTP 응답 코드 처리
  return children({ data });
};
```

```tsx
<Query url={`https://dog.ceo/api/breed/${breed}/images/random`}>
  {({ data }) => {
    const imageSrc = get(data, "message");
    return (
      <div>
        <div>
          <Link to="/">back</Link>
        </div>
        {!imageSrc && <p>Loading...</p>}
        {imageSrc && (
          <img alt={`A nice ${breed}`} src={imageSrc} height={200} />
        )}
      </div>
    );
  }}
</Query>
```

## 패턴 구현하기

    1. 애러 상태를 관리하는 top-level component 를 만든다.
    - 이 컴포넌트는 하위(깊숙한) 컴포넌트로 부터 현재의 애러상태를 보고 받을 수 있다.
    - 현재 애러상태라면 , 애러 코드에 맞게 404,405,500 등의 페이지를 보여준다.
    - 현재 애러상태가 아니라면, children 을 랜더링 한다.

    2. 위 chilren 은 Router 및 Route 의 코드가 있다.
    - url 정규식에서 일치하지 않을때 404페이지를 랜더링 되도록 해야함
    - swith 컴포넌트 마지막에는 404 page 컴포넌트를 Route 해준다.


    3. Query 관련 훅 만들기
    - axios , fetch 든 데이터를 넘겨주기전에, 애러가 발생한다면
    - 서버상태 애러를 관리하는 top-level component에게 보고해서

-랜더링 페이지를 애러상태코드로 바꾸도록 한다.
